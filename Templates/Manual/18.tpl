<h1><img class="unit u8" src="img/x.gif" alt="Fire Catapult" title="Fire Catapult" /> Fire Catapult <span class="tribe">（<?php echo ROMANS;?>）</span></h1>

<table id="troop_info" cellpadding="1" cellspacing="1">
<thead><tr>
	<th><img class="att_all" src="img/x.gif" alt="attack value" title="attack value" /></th>
	<th><img class="def_i" src="img/x.gif" alt="defence against infantry" title="defence against infantry" /></th>
	<th><img class="def_c" src="img/x.gif" alt="defence against cavalry" title="defence against cavalry" /></th><th><img class="r1" src="img/x.gif" alt="Lumber" title="Lumber" /></th><th><img class="r2" src="img/x.gif" alt="Clay" title="Clay" /></th><th><img class="r3" src="img/x.gif" alt="Iron" title="Iron" /></th><th><img class="r4" src="img/x.gif" alt="Crop" title="Crop" /></th></tr></thead>
<tbody><tr>
	<td>75</td>
	<td>60</td>
	<td>10</td>

	<td>950</td>
	<td>1350</td>
	<td>600</td>
	<td>90</td>
</tr></tbody>
</table>

<table id="troop_details" cellpadding="1" cellspacing="1">
<tbody><tr>

	<th><?php echo VELOCITY;?></th>
	<td><b>3</b> <?php echo Fields_PER_HOUR;?></td>
</tr>
<tr>
	<th><?php echo CAN_CARRY;?></th>
	<td><b>0</b> <?php echo RESOURCES;?></td>

</tr>
<tr>
	<th><?php echo UPKEEP;?></th>
	<td><img class="r5" src="img/x.gif" alt="Crop consumption" title="Crop consumption" /> 6</td>
</tr>
<tr>
	<th><?php echo DURATION_OF_TRAINING;?></th>
	<td><img class="clock" src="img/x.gif" alt="duration" title="duration" /> 2:30:00</td>

</tr>
</tbody></table>

<img id="big_unit" class="big_u8" src="img/x.gif" alt="Fire Catapult" title="Fire Catapult" /><div id="t_desc">The Catapult is an excellent long-distance weapon; it is used to destroy the fields and buildings of enemy villages. However, without escorting troops it is almost defenceless so don't forget to send some of your troops with it. 
<br /><br />
Having a high level rally point makes your catapults more accurate and gives you the option to target additional enemy buildings. With a level 10 rally point each building except for the cranny, stonemason's lodge and trapper can be targeted. 
<br />
HINT: Fire catapults CAN hit the cranny, trappers or stonemason's lodge when they target randomly.</div><div id="prereqs"><b><?php echo PREREQUISITES;?></b><br /><a href="manual.php?typ=4&amp;gid=21">Workshop</a> <?php echo LEVEL;?> 10, <a href="manual.php?typ=4&amp;gid=22">Academy</a> <?php echo LEVEL;?> 15</div><map id="nav" name="nav">
 <area href="manual.php?typ=1&amp;s=7" title="back" coords="0,0,45,18" shape="rect" alt="" />

 <area href="manual.php?s=1" title="Overview" coords="46,0,70,18" shape="rect" alt="" />
 <area href="manual.php?typ=1&amp;s=9" title="forward" coords="71,0,116,18" shape="rect" alt="" />
</map>
<img usemap="#nav" src="img/x.gif" class="navi" alt="" />
