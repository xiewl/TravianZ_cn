<h1><img class="unit u1" src="img/x.gif" alt="Legionnaire" title="Legionnaire" /> <?php echo U1 ;?> <span class="tribe">（<?php echo ROMANS;?>）</span></h1>

<table id="troop_info" cellpadding="1" cellspacing="1">
<thead><tr>
	<th><img class="att_all" src="img/x.gif" alt="attack value" title="attack value" /></th>
	<th><img class="def_i" src="img/x.gif" alt="defence against infantry" title="defence against infantry" /></th>
	<th><img class="def_c" src="img/x.gif" alt="defence against cavalry" title="defence against cavalry" /></th><th><img class="r1" src="img/x.gif" alt="Lumber" title="Lumber" /></th><th><img class="r2" src="img/x.gif" alt="Clay" title="Clay" /></th><th><img class="r3" src="img/x.gif" alt="Iron" title="Iron" /></th><th><img class="r4" src="img/x.gif" alt="Crop" title="Crop" /></th></tr></thead>
<tbody><tr>
	<td>40</td>
	<td>35</td>
	<td>50</td>

	<td>120</td>
	<td>100</td>
	<td>150</td>
	<td>30</td>
</tr></tbody>
</table>

<table id="troop_details" cellpadding="1" cellspacing="1">
<tbody><tr>

	<th><?php echo VELOCITY;?></th>
	<td><b>6</b> <?php echo Fields_PER_HOUR;?></td>
</tr>
<tr>
	<th><?php echo CAN_CARRY;?></th>
	<td><b>50</b> <?php echo RESOURCES;?></td>
</tr>
<tr>
	<th><?php echo UPKEEP;?></th>
	<td><img class="r5" src="img/x.gif" alt="Crop consumption" title="Crop consumption" /> 1</td>
</tr>
<tr>
	<th><?php echo DURATION_OF_TRAINING;?></th>
	<td><img class="clock" src="img/x.gif" alt="duration" title="duration" /> 0:26:40</td>

</tr>
</tbody></table>

<img id="big_unit" class="big_u1" src="img/x.gif" alt="Legionnaire" title="Legionnaire" /><div id="t_desc"><?php echo U1_DESC;?></div><div id="prereqs"><b><?php echo PREREQUISITES;?></b><br /><a href="manual.php?typ=4&amp;gid=19"><?php echo BARRACKS;?></a> <?php echo LEVEL;?> 1</div><map id="nav" name="nav">
 <area href="manual.php?s=1" title="back" coords="0,0,45,18" shape="rect" alt="" />
 <area href="manual.php?s=1" title="Overview" coords="46,0,70,18" shape="rect" alt="" />
 <area href="manual.php?typ=1&amp;s=2" title="forward" coords="71,0,116,18" shape="rect" alt="" />
</map>
<img usemap="#nav" src="img/x.gif" class="navi" alt="" />
