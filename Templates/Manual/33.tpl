<h1><img class="unit ugeb" src="img/x.gif" alt="" title=""/> <?php echo THE_BUILDINGS;?>（<?php echo INFRASTRUCTURE;?>）</h1>
<ul>
    <li><a href="manual.php?typ=4&amp;gid=15"><?php echo MAINBUILDING;?></a></li>
    <li><a href="manual.php?typ=4&amp;gid=17"><?php echo MARKETPLACE;?></a></li>
    <li><a href="manual.php?typ=4&amp;gid=18"><?php echo EMBASSY;?></a></li>
    <li><a href="manual.php?typ=4&amp;gid=23"><?php echo CRANNY;?></a></li>
    <li><a href="manual.php?typ=4&amp;gid=24"><?php echo TOWNHALL;?></a></li>
    <li><a href="manual.php?typ=4&amp;gid=25"><?php echo RESIDENCE;?></a></li>
    <li><a href="manual.php?typ=4&amp;gid=26"><?php echo PALACE;?></a></li>
    <li><a href="manual.php?typ=4&amp;gid=27"><?php echo TREASURY;?></a></li>
    <li><a href="manual.php?typ=4&amp;gid=28"><?php echo TRADEOFFICE;?></a></li>
    <li><a href="manual.php?typ=4&amp;gid=34"><?php echo STONEMASON;?></a></li>
    <li><a href="manual.php?typ=4&amp;gid=35"><?php echo BREWERY;?></a></li>
    <li><a href="manual.php?typ=4&amp;gid=38"><?php echo GREATWAREHOUSE;?></a></li>
    <li><a href="manual.php?typ=4&amp;gid=39"><?php echo GREATGRANARY;?></a></li>
    <li><a href="manual.php?typ=4&amp;gid=41"><?php echo HORSEDRINKING;?></a></li>
    <li><a href="manual.php?typ=4&amp;gid=40"><?php echo WONDER;?></a></li>
</ul>
<map id="nav" name="nav">

    <area href="manual.php?typ=3&amp;s=2" title="back" coords="0,0,45,18" shape="rect" alt=""/>
    <area href="manual.php?s=1" title="Overview" coords="46,0,70,18" shape="rect" alt=""/>
    <area href="manual.php?typ=3&amp;s=1" title="forward" coords="71,0,116,18" shape="rect" alt=""/>
</map>
<img usemap="#nav" src="img/x.gif" class="navi" alt=""/>
