<h1><img class="unit u2" src="img/x.gif" alt="Praetorian" title="Praetorian" /> <?php echo U2 ;?> <span class="tribe">（<?php echo ROMANS;?>）</span></h1>

<table id="troop_info" cellpadding="1" cellspacing="1">
<thead><tr>
	<th><img class="att_all" src="img/x.gif" alt="attack value" title="attack value" /></th>
	<th><img class="def_i" src="img/x.gif" alt="defence against infantry" title="defence against infantry" /></th>
	<th><img class="def_c" src="img/x.gif" alt="defence against cavalry" title="defence against cavalry" /></th><th><img class="r1" src="img/x.gif" alt="Lumber" title="Lumber" /></th><th><img class="r2" src="img/x.gif" alt="Clay" title="Clay" /></th><th><img class="r3" src="img/x.gif" alt="Iron" title="Iron" /></th><th><img class="r4" src="img/x.gif" alt="Crop" title="Crop" /></th></tr></thead>
<tbody><tr>
	<td>30</td>
	<td>65</td>
	<td>35</td>

	<td>100</td>
	<td>130</td>
	<td>160</td>
	<td>70</td>
</tr></tbody>
</table>

<table id="troop_details" cellpadding="1" cellspacing="1">
<tbody><tr>
	<th><?php echo VELOCITY;?></th>
	<td><b>5</b> <?php echo Fields_PER_HOUR;?></td>
</tr>
<tr>
	<th><?php echo CAN_CARRY;?></th>
	<td><b>20</b> <?php echo RESOURCES;?></td>
</tr>
<tr>
	<th><?php echo UPKEEP;?></th>
	<td><img class="r5" src="img/x.gif" alt="Crop consumption" title="Crop consumption" /> 1</td>
</tr>
<tr>
	<th><?php echo DURATION_OF_TRAINING;?></th>
	<td><img class="clock" src="img/x.gif" alt="duration" title="duration" /> 0:29:20</td>
</tr>
</tbody></table>

<img id="big_unit" class="big_u2" src="img/x.gif" alt="Praetorian" title="Praetorian" /><div id="t_desc"><?php echo U2_DESC;?></div><div id="prereqs"><b><?php echo PREREQUISITES;?></b><br /><a href="manual.php?typ=4&amp;gid=22"><?php echo ACADEMY;?></a> <?php echo LEVEL;?> 1, <a href="manual.php?typ=4&amp;gid=13">Armoury</a> <?php echo LEVEL;?> 1</div><map id="nav" name="nav">
 <area href="manual.php?typ=1&amp;s=1" title="back" coords="0,0,45,18" shape="rect" alt="" />
 <area href="manual.php?s=1" title="Overview" coords="46,0,70,18" shape="rect" alt="" />
 <area href="manual.php?typ=1&amp;s=3" title="forward" coords="71,0,116,18" shape="rect" alt="" />

</map>
<img usemap="#nav" src="img/x.gif" class="navi" alt="" />
