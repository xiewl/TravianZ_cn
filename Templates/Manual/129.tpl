<h1><img class="unit u29" src="img/x.gif" alt="Chieftain" title="Chieftain" /> Chieftain <span class="tribe">(Gauls)</span></h1>

<table id="troop_info" cellpadding="1" cellspacing="1">
<thead><tr>
	<th><img class="att_all" src="img/x.gif" alt="attack value" title="attack value" /></th>
	<th><img class="def_i" src="img/x.gif" alt="defence against infantry" title="defence against infantry" /></th>
	<th><img class="def_c" src="img/x.gif" alt="defence against cavalry" title="defence against cavalry" /></th><th><img class="r1" src="img/x.gif" alt="Lumber" title="Lumber" /></th><th><img class="r2" src="img/x.gif" alt="Clay" title="Clay" /></th><th><img class="r3" src="img/x.gif" alt="Iron" title="Iron" /></th><th><img class="r4" src="img/x.gif" alt="Crop" title="Crop" /></th></tr></thead>
<tbody><tr>
	<td>40</td>
	<td>50</td>
	<td>50</td>

	<td>30750</td>
	<td>45400</td>
	<td>31000</td>
	<td>37500</td>
</tr></tbody>
</table>

<table id="troop_details" cellpadding="1" cellspacing="1">
<tbody><tr>

	<th><?php echo VELOCITY;?></th>
	<td><b>5</b> <?php echo Fields_PER_HOUR;?></td>
</tr>
<tr>
	<th><?php echo CAN_CARRY;?></th>
	<td><b>0</b> <?php echo RESOURCES;?></td>

</tr>
<tr>
	<th><?php echo UPKEEP;?></th>
	<td><img class="r5" src="img/x.gif" alt="Crop consumption" title="Crop consumption" /> 4</td>
</tr>
<tr>
	<th><?php echo DURATION_OF_TRAINING;?></th>
	<td><img class="clock" src="img/x.gif" alt="duration" title="duration" /> 25:11:40</td>

</tr>
</tbody></table>

<img id="big_unit" class="big_u29" src="img/x.gif" alt="Chieftain" title="Chieftain" /><div id="t_desc">Each tribe has an ancient and experienced fighter whose presence and speeches are able to convince the population of enemy villages to join his tribe.
<br /><br />
The more often the Chieftain speaks in front of the walls of an enemy village the more its loyalty sinks until it joins the Chieftain's tribe.</div><div id="prereqs"><b><?php echo PREREQUISITES;?></b><br /><a href="manual.php?typ=4&amp;gid=16">Rally Point</a> <?php echo LEVEL;?> 10, <a href="manual.php?typ=4&amp;gid=22">Academy</a> <?php echo LEVEL;?> 20</div><map id="nav" name="nav">
 <area href="manual.php?typ=1&amp;s=28" title="back" coords="0,0,45,18" shape="rect" alt="" />
 <area href="manual.php?s=1" title="Overview" coords="46,0,70,18" shape="rect" alt="" />

 <area href="manual.php?typ=1&amp;s=30" title="forward" coords="71,0,116,18" shape="rect" alt="" />
</map>
<img usemap="#nav" src="img/x.gif" class="navi" alt="" />
