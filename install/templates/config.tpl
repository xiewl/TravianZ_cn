<?php

#################################################################################
##              -= YOU MAY NOT REMOVE OR CHANGE THIS NOTICE =-                 ##
## --------------------------------------------------------------------------- ##
##  Project:       TravianZ                                                    ##
##  Version:       22.06.2015                    			       ## 
##  Filename       config.tpl                                                  ##
##  Developed by:  Mr.php , Advocaite , brainiacX , yi12345 , Shadow , ronix   ## 
##  Fixed by:      Shadow - STARVATION , HERO FIXED COMPL.  		       ##
##  Fixed by:      InCube - double troops				       ##
##  License:       TravianZ Project                                            ##
##  Copyright:     TravianZ (c) 2010-2015. All rights reserved.                ##
##  URLs:          http://travian.shadowss.ro                		       ##
##  Source code:   https://github.com/Shadowss/TravianZ		               ## 
##                                                                             ##
#################################################################################

if(isset($_GET['c']) && $_GET['c'] == 1) {
echo "<div class=\"headline\"><span class=\"f10 c5\">Error creating constant.php check cmod.</span></div><br>";
}
?>

<form action="process.php" method="post" id="dataform">

	<p>
	<span class="f10 c">服务器相关</span>
	<table><tr>
	<td><span class="f9 c6">服务器名称：</span></td><td width="140"><input type="text" name="servername" id="servername" value="TravianZ"></td></tr><tr>
	        <td><span class="f9 c6">服务器时区：</span></td><td>
	<select name="tzone" onChange="refresh(this.value)">
	<option value="1,Africa/Dakar" <?php if ($tz==1) echo "selected";?>>Africa</option>
	<option value="2,America/New_York" <?php if ($tz==2) echo "selected";?>>America</option>
	<option value="3,Antarctica/Casey" <?php if ($tz==3) echo "selected";?>>Antarctica</option>
	<option value="4,Arctic/Longyearbyen" <?php if ($tz==4) echo "selected";?>>Arctic</option>
	<option value="5,Asia/Kuala_Lumpur" <?php if ($tz==5) echo "selected";?>>北京时间</option>
	<option value="6,Atlantic/Azores" <?php if ($tz==6) echo "selected";?>>Atlantic</option>
	<option value="7,Australia/Melbourne" <?php if ($tz==7) echo "selected";?>>Australia</option>
	<option value="8,Europe/Bucharest" <?php if ($tz==8) echo "selected";?>>Europe (Bucharest)</option>
        <option value="9,Europe/London" <?php if ($tz==9) echo "selected";?>>Europe (London)</option>
	<option value="10,Indian/Maldives" <?php if ($tz==10) echo "selected";?>>Indian</option>
	<option value="11,Pacific/Fiji" <?php if ($tz==11) echo "selected";?>>Pacific</option>
	</select>
        </td></tr><tr>
	<td><span class="f9 c6">服务器速度：</span></td><td><input name="speed" type="text" id="speed" value="1" size="2"></td></tr><tr>
	<td><span class="f9 c6">军队速度：</span></td><td width="140"><input type="text" name="incspeed" id="incspeed" value="1" size="2"></td></tr><tr>
	<td><span class="f9 c6">回避速度：</span></td><td><input name="evasionspeed" type="text" id="evasionspeed" value="1" size="2"></td></tr><tr>
	<td><span class="f9 c6">商人交易比例：</span></td><td width="140"><input type="text" name="tradercap" id="tradercap" value="1" size="2"></td></tr><tr>
	<td><span class="f9 c6">山洞容量倍数：</span></td><td width="140"><input type="text" name="crannycap" id="crannycap" value="1" size="2"></td></tr><tr>
	<td><span class="f9 c6">陷阱机容量倍数：</span></td><td width="140"><input type="text" name="trappercap" id="trappercap" value="1" size="2"></td></tr><tr>
	<td><span class="f9 c6">纳塔族单位倍数：</span></td><td width="140"><input type="text" name="natars_units" id="natars_units" value="100" size="3"></td></tr><tr>
	<td><span class="f9 c6">世界大小：</span></td><td>
				<select name="wmax">
				<option value="10">10x10</option>
				<option value="25">25x25</option>
				<option value="50" selected="selected">50x50</option>
				<option value="100">100x100</option>
				<option value="150">150x150</option>
				<option value="200">200x200</option>
				<option value="250">250x250</option>
				<option value="300">300x300</option>
				<option value="350">350x350</option>
				<option value="400">400x400</option>
				</select>
		</td></tr><tr></tr>
	<td><span class="f9 c6">开放注册：</span></td><td>
				<select name="reg_open">
				<option value="True" selected="selected">是</option>
				<option value="False">否</option>
				</select>
		</td></tr><tr></tr>
	<td><span class="f9 c6">Server:</span></td><td><input name="server" type="text" id="homepage" value="http://<?php echo $_SERVER['HTTP_HOST']; ?>/"></td></tr><tr></tr>
	<td><span class="f9 c6">Domain:</span></td><td><input name="domain" type="text" id="homepage" value="http://<?php echo $_SERVER['HTTP_HOST']; ?>/"></td></tr><tr></tr>
	<td><span class="f9 c6">Homepage:</span></td><td><input name="homepage" type="text" id="homepage" value="http://<?php echo $_SERVER['HTTP_HOST']; ?>/"></td></tr><tr></tr>
	<td><span class="f9 c6">语言：</span></td><td>
				<select name="lang">
					<option value="zh-cn" selected="selected">Chinese</option>
					<option value="zh_tw">Taiwan</option>
					<option value="en">English</option>
				</select>
		</td></tr><tr></tr>
	<td><span class="f9 c6">游戏保护时长：</span></td><td>
				<select name="beginner">
				<option value="7200" selected="selected">2 hours</option>
				<option value="10800">3 hours</option>
				<option value="18000">5 hours</option>
				<option value="28800">8 hours</option>
				<option value="36000">10 hours</option>
				<option value="43200">12 hours</option>
				<option value="86400">24 hours (1 day)</option>
				<option value="172800">48 hours (2 days)</option>
				<option value="259200">72 hours (3 days)</option>
				<option value="432000">120 hours (5 days)</option>
				</select>
		</td></tr><tr></tr>
	<td><span class="f9 c6">Plus账号时长：</span></td><td>
				<select name="plus_time">
				<option value="(3600*12)">12 hours</option>
				<option value="(3600*24)">1 day</option>
				<option value="(3600*24*2)">2 days</option>
				<option value="(3600*24*3)">3 days</option>
				<option value="(3600*24*4)">4 days</option>
				<option value="(3600*24*5)">5 days</option>
				<option value="(3600*24*6)">6 days</option>
				<option value="(3600*24*7)" selected="selected">7 days</option>
				</select>
		</td></tr><tr></tr>
	<td><span class="f9 c6">+25%产量时长：</span></td><td>
				<select name="plus_production">
				<option value="(3600*12)">12 hours</option>
				<option value="(3600*24)">1 day</option>
				<option value="(3600*24*2)">2 days</option>
				<option value="(3600*24*3)">3 days</option>
				<option value="(3600*24*4)">4 days</option>
				<option value="(3600*24*5)">5 days</option>
				<option value="(3600*24*6)">6 days</option>
				<option value="(3600*24*7)" selected="selected">7 days</option>
				</select>
		</td></tr><tr></tr>
	<td><span class="f9 c6">自然生物再生间隔时间：</span></td><td>
				<select name="nature_regtime">
				<option value="3600" selected="selected">1 hours</option>
				<option value="28800">8 hours</option>
				<option value="36000">10 hours</option>
				<option value="43200">12 hours</option>
				<option value="57600">16 hours</option>
				<option value="72000">20 hours</option>
				<option value="86400">24 hours</option>
				</select>
		</td></tr><tr></tr>
    <tr class="hover">
  	<td><span class="f9 c6">奖牌结算间隔时间：</span></td><td>
        <select name="medalinterval">
        <option value="0">none</option>
        <option value="(3600*24)">1 day</option>
        <option value="(3600*24*2)">2 days</option>
        <option value="(3600*24*3)">3 days</option>
        <option value="(3600*24*4)">4 days</option>
        <option value="(3600*24*5)">5 days</option>
        <option value="(3600*24*6)">6 days</option>
        <option value="(3600*24*7)" selected="selected">7 days</option>
        </select>
    	</td></tr><tr></tr> 
	<td><span class="f9 c6">仓库/粮仓容量倍数：</span></td><td width="140"><input type="text" name="storage_multiplier" id="storage_multiplier" value="1"></td></tr><tr>
	<td><span class="f9 c6">竞技场生效最小格子数：</span></td><td width="140"><input type="text" name="ts_threshold" id="ts_threshold" value="5"></td></tr><tr>
	<td><span class="f9 c6">开启大型仓库：</span></td><td>
				<select name="great_wks">
				<option value="True" selected="selected">是</option>
				<option value="False">否</option>
				</select>
		</td></tr><tr></tr>
	<td><span class="f9 c6">开启货栈单统计：</span></td><td>
				<select name="ww">
				<option value="True" selected="selected">是</option>
				<option value="False">否</option>
				</select>
		</td></tr><tr></tr>
	<td><span class="f9 c6">纳塔族村庄加入统计排名：</span></td><td>
				<select name="show_natars">
				<option value="True" selected="selected">是</option>
				<option value="False">否</option>
				</select>
		</td></tr><tr></tr>
	<td><span class="f9 c6">合平系统：</span></td><td>
				<select name="peace">
				<option value="0" selected="selected">None</option>
				<option value="1">Normal</option>
				<option value="2">Christmas</option>
				<option value="3">New Year</option>
				<option value="4">Easter</option>
				</select>
		</td></tr><tr></tr>
		</table>
	</p>

		<p>
	<span class="f10 c">管理员账号</span>
	<table>
	<tr><td><span class="f9 c6">管理员名字：</span></td><td><input type="text" name="aname" id="aname" value="admin"></td></tr>
	<tr><td><span class="f9 c6">管理员邮箱：</span></td><td><input type="text" name="aemail" id="aemail" value="admin@admin.com"></td></tr>

	<td><span class="f9 c6">管理员村庄加入统计排名：</span></td><td>
				<select name="admin_rank">
				<option value="True" selected="selected">是</option>
				<option value="False">否</option>
				</select>
		</td></tr><tr></tr>
	</table>
	</p>
	<p>
	<span class="f10 c">数据库连接串</span>
	<table><tr>
	<td><span class="f9 c6">主机地址：</span></td><td><input name="sserver" type="text" id="sserver" value="rds28qv58v0h154oh22u.mysql.rds.aliyuncs.com"></td></tr><tr>
	<td><span class="f9 c6">数据库用户名：</span></td><td><input name="suser" type="text" id="suser" value="r130oa92si386q95"></td></tr><tr>
	<td><span class="f9 c6">数据库密码：</span></td><td><input type="password" name="spass" id="spass" value="bnm123456"></td></tr><tr>
	<td><span class="f9 c6">数据库名称：</span></td><td><input type="text" name="sdb" id="sdb" value="r130oa92si386q95"></td></tr><tr>
	<td><span class="f9 c6">数据表前缀：</span></td><td><input type="text" name="prefix" id="prefix" value="s1_" size="5"></td></tr>
	<td><span class="f9 c6">数据库类型：</span></td><td><select name="connectt">
	  <option value="0" selected="selected">MYSQL</option>
	  <option value="1">MYSQLi</option>
	</select></td></tr>
	</table>
	</p>
	<p>
	<span class="f10 c">NEWSBOX OPTIONS</span>
	<table><tr>
	<td><span class="f9 c6">Newsbox 1:</span></td><td><select name="box1">
	  <option value="true">Enabled</option>
	  <option value="false" selected="selected">Disabled</option>
	</select></td></tr>
	<td><span class="f9 c6">Newsbox 2:</span></td><td><select name="box2">
	  <option value="true">Enabled</option>
	  <option value="false" selected="selected">Disabled</option>
	</select></td></tr>
	<td><span class="f9 c6">Newsbox 3:</span></td><td><select name="box3">
	  <option value="true">Enabled</option>
	  <option value="false" selected="selected">Disabled</option>
	</select></td></tr>
	</table>
	</p>
	<p>
	<span class="f10 c">LOG RELATED (You should disable them)</span>
	<table><tr>
	<td><span class="f9 c6">Log Building:</span></td><td><select name="log_build">
	  <option value="true">Yes</option>
	  <option value="false" selected="selected">No</option>
	</select></td></tr><tr>
	<td><span class="f9 c6">Log Tech:</span></td><td><select name="log_tech">
	  <option value="true">Yes</option>
	  <option value="false" selected="selected">No</option>
	</select></td></tr><tr>
	<td><span class="f9 c6">Log Login:</span></td><td><select name="log_login">
	  <option value="true">Yes</option>
	  <option value="false" selected="selected">No</option>
	</select></td></tr>
 	 <tr>
	<td><span class="f9 c6">Log Gold:</span></td><td><select name="log_gold_fin">
	  <option value="true">Yes</option>
	  <option value="false" selected="selected">No</option>
	</select></td></tr>
	<tr>	<td><span class="f9 c6">Log Admin:</span></td><td><select name="log_admin">
	  <option value="true">Yes</option>
	  <option value="false" selected="selected">No</option>
	</select></td></tr>
	<tr>	<td><span class="f9 c6">Log War:</span></td><td><select name="log_war">
	  <option value="true">Yes</option>
	  <option value="false" selected="selected">No</option>
	</select></td></tr><tr>
	<td><span class="f9 c6">Log Market:</span></td><td><select name="log_market">
	  <option value="true">Yes</option>
	  <option value="false" selected="selected">No</option>
	</select></td></tr><tr>
	<td><span class="f9 c6">Log Illegal:</span></td><td><select name="log_illegal">
	  <option value="true">Yes</option>
	  <option value="false" selected="selected">No</option>
	</select></td></tr><tr>
	<td><span class="f9 c6">Log :</span></td><td><select name="">
	  <option value="true">Yes</option>
	  <option value="false" selected="selected">No</option>
	</select></td></tr>
	</table>
	</p>

	<p>
    <span class="f10 c">其它设置</span>
    <table><tr>
    <td><span class="f9 c6">开启新手任务：</span></td><td><select name="quest">
      <option value="true" selected="selected">是</option>
      <option value="false">否</option>
    </select></td></tr><tr>
        <td><span class="f9 c6">任务类型</span></td><td><select name="qtype">
      <option value="25" selected="selected">官方任务</option>
      <option value="37">TravianZ扩展任务</option>
    </select></td></tr><tr>
	<td><span class="f9 c6">需要激活邮箱：</span></td><td><select name="activate">
	  <option value="true">是</option>
	  <option value="false" selected="selected">否</option>
	</select></td></tr><tr>
	<td><span class="f9 c6">Limit Mailbox:</span></td><td><select name="limit_mailbox">
	  <option value="true">Yes</option>
	  <option value="false" selected="selected">No</option>
	</select> (NOT DONE)</td></tr>
	<tr><td><span class="f9 c6">Max mails:</span></td><td><input type="text" name="max_mails" id="max_mails" value="30" size="4"> (NOT DONE)</td></tr>
	 <tr>
	<td><span class="f9 c6">拆除功能最低级要求:</span></td><td><select name="demolish">
			<option value="5">5</option>
			<option value="10" selected="selected">10 - Default</option>
			<option value="15">15</option>
			<option value="20">20</option>
	</select></td></tr>
	<tr>	<td><span class="f9 c6">村庄开发速度：</span></td><td><select name="village_expand">
	  <option value="1" selected="selected">慢</option>
	  <option value="0">快</option>
	</select></td></tr>
	<tr>	<td><span class="f9 c6">Error Reporting:</span></td><td><select name="error">
	  <option value="error_reporting (E_ALL ^ E_NOTICE);" selected="selected">Yes</option>
	  <option value="error_reporting (0);">No</option>
	</select></td></tr><tr>
	<td><span class="f9 c6">T4 is Coming screen:</span></td><td><select name="t4_coming">
	  <option value="true">Yes</option>
	  <option value="false" selected="selected">No</option>
	</select></td></tr>
	</table>
	</p>
<br />
	<span class="f10 c">服务器开启设定</span>
	<table>
	<tr><td><span class="f9 c6">开始日期：</span></td><td width="140"><input type="text" name="start_date" id="start_date" value="<?php echo date('m/d/Y'); ?>"></td></tr>
	<tr><td><span class="f9 c6">开始时间：</span></td><td width="140"><input type="text" name="start_time" id="start_time" value="<?php echo date('H:i'); ?>"></td></tr>
	</table>
	<center>
	<input type="submit" name="Submit" id="Submit" value="下一步">
	<input type="hidden" name="subconst" value="1"></center>
	</form>
</div>
