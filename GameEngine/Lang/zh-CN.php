<?php

//////////////////////////////////////////////////////////////////////////////////////////////////////
//                                             TRAVIANX                                             //
//            Only for advanced users, do not edit if you dont know what are you doing!             //
//                                Made by: Dzoki & Dixie (TravianX)                                 //
//                              - TravianX = Travian Clone Project -                                //
//                                 DO NOT REMOVE COPYRIGHT NOTICE!                                  //
//                                Adding tasks, constructions and artefact  by: Armando             //
//////////////////////////////////////////////////////////////////////////////////////////////////////
									//                         //
									//         zh-cn         //
									//      Author: Taozi      //
									/////////////////////////////

//MAIN MENU
define("TRIBE1","罗马人");
define("TRIBE2","日耳曼人");
define("TRIBE3","高卢人");
define("TRIBE4","自然界");
define("TRIBE5","纳塔族");
define("TRIBE6","怪物");

define("HOME","首页");
define("INSTRUCT","说明");
define("ADMIN_PANEL","管理员控制台-ACP");
define("MASS_MESSAGE","群发消息");
define("LOGOUT","注销");
define("PROFILE","个人资料");
define("SUPPORT","支持");
define("UPDATE_T_10","更新前10名");
define("SYSTEM_MESSAGE","系統公告");
define("TRAVIAN_PLUS","Travian <b><span class=\"plus_g\">P</span><span class=\"plus_o\">l</span><span class=\"plus_g\">u</span><span class=\"plus_o\">s</span></span></span></b>");
define("CONTACT","联系我们");
define("GAME_RULES","游戏规则");

//MENU
define("REG","注册");
define("FORUM","论坛");
define("CHAT","聊天室");
define("IMPRINT","版权");
define("MORE_LINKS","更多链接");
define("TOUR","游戏之旅");


//ERRORS
define("USRNM_EMPTY","（用户名为空）");
define("USRNM_TAKEN","（用户名已被使用）");
define("USRNM_SHORT","（用户名最少为".USRNM_MIN_LENGTH."位数）");
define("USRNM_CHAR","（无效字符）");
define("PW_EMPTY","（密码为空）");
define("PW_SHORT","（密码最少为".PW_MIN_LENGTH."位数）");
define("PW_INSECURE","(密码不安全，请输入一个安全的密码。");
define("EMAIL_EMPTY","（邮箱为空）");
define("EMAIL_INVALID","（无效的邮箱地址）");
define("EMAIL_TAKEN","（邮箱已被注册）");
define("TRIBE_EMPTY","<li>请选择一个种族。</li>");
define("AGREE_ERROR","<li>你必须接受本游戏的游戏规定和条款才能完成注册。</li>");
define("LOGIN_USR_EMPTY","输入用户名。");
define("LOGIN_PASS_EMPTY","输入密码。");
define("EMAIL_ERROR","Email does not match existing");
define("PASS_MISMATCH","Passwords do not match");
define("ALLI_OWNER","Please appoint an alliance owner before deleting");
define("SIT_ERROR","Sitter already set");
define("USR_NT_FOUND","用户名不存在。");
define("LOGIN_PW_ERROR","密码错误。");
define("WEL_TOPIC","Useful tips & information ");
define("ATAG_EMPTY","Tag empty");
define("ANAME_EMPTY","Name empty");
define("ATAG_EXIST","Tag taken");
define("ANAME_EXIST","Name taken");
define("NOT_OPENED_YET","Server not started yet.");
define("REGISTER_CLOSED","The register is closed. You can't register to this server.");
define("NAME_EMPTY","Please insert name");
define("NAME_NO_EXIST","There is no user with the name ");
define("ID_NO_EXIST","There is no user with the id ");
define("SAME_NAME","You can't invite yourself");
define("ALREADY_INVITED"," already invited");
define("ALREADY_IN_ALLY"," already in this alliance");

//COPYRIGHT
define("TRAVIAN_COPYRIGHT","TravianZ 100% Open Source Travian Clone.");

//BUILD.TPL
define("CUR_PROD","当前的产量");
define("NEXT_PROD","产量在等級 ");

//BUILDINGS
define("B1","伐木场");
define("B1_DESC","伐木场是砍伐木材的地方.伐木场等級越高,单位時間内能砍伐的木材也就越多.");
define("B2","泥坑");
define("B2_DESC","泥坑是生产泥土的地方.泥坑等級越高,单位時間內生产的砖块也就越多.");
define("B3","铁矿场");
define("B3_DESC","铁矿场是用來采集铁矿的.铁矿场等級越高,每小時能采集的铁矿就越多.");
define("B4","农场");
define("B4_DESC","农场是为居民生产粮食的地方.农场等級越高,所能生产的粮食也就越多.");

//DORF1
define("LUMBER","木材");
define("CLAY","砖块");
define("IRON","钢铁");
define("CROP","粮食");
define("LEVEL","等级");
define("CROP_COM",CROP."消耗");
define("PER_HR","每小时");
define("PROD_HEADER","生产");
define("MULTI_V_HEADER","村庄");
define("ANNOUNCEMENT","公告");
define("GO2MY_VILLAGE","回到我的村庄");
define("VILLAGE_CENTER","地图中心");
define("FINISH_GOLD","花费2金,完成这个村庄当前建筑任务和兵种研发、升级的任务。（除了皇宮和行宮）");
define("WAITING_LOOP","（等待建筑）");
define("HRS","（小时）");
define("DONE_AT","完成于");
define("CANCEL","取消");
define("LOYALTY","忠诚度：");
define("CALCULATED_IN","产生在");
define("SEVER_TIME","服务器时间：");

//QUEST
define("Q_CONTINUE","继续下一个任务");
define("Q_REWARD","你的奖励：");
define("Q_BUTN","完成任务");
define("Q0","欢迎来到");
define("Q0_DESC","恭喜你成为了这个小村庄的首领。我将成为你的引导人，帮你快速上手这个游戏，祝你游戏愉快！");
define("Q0_OPT1","开始第一个任务");
define("Q0_OPT2","先看看四周环境");
define("Q0_OPT3","我不做任务");

define("Q1","任务 1: 伐木场");
define("Q1_DESC","There are four green forests around your village. Construct a woodcutter on one of them. Lumber is an important resource for our new settlement.");
define("Q1_ORDER","Order:<\/p>Construct a woodcutter.");
define("Q1_RESP","Yes, that way you gain more lumber.I helped a bit and completed the order instantly.");
define("Q1_REWARD","Woodcutter instantly completed.");

define("Q2","任务 2: 粮食");
define("Q2_DESC","Now your subjects are hungry from working all day. Extend a cropland to improve your subjects' supply. Come back here once the building is complete.");
define("Q2_ORDER","Order:<\/p>Extend one cropland.");
define("Q2_RESP","Very good. Now your subjects have enough to eat again...");
define("Q2_REWARD","Your reward:<\/p>1 day Travian");

define("Q3","任务 3: 你的村庄名");
define("Q3_DESC","Creative as you are you can grant your village the ultimate name.<br \/><br \/>Click on 'profile' in the left hand menu and then select 'change profile'...");
define("Q3_ORDER","Order:<\/p>Change your village's name to something nice.");
define("Q3_RESP","Wow, a great name for their village. It could have been the name of my village!...");

define("Q4","任务 4: 其他玩家");
define("Q4_DESC","In ". SERVER_NAME ." you play along with billions of other players. Click 'statistics' in the top menu to look up your rank and enter it here.");
define("Q4_ORDER","Order:<\/p>Look for your rank in the statistics and enter it here.");
define("Q4_BUTN","complete task");
define("Q4_RESP","Exactly! That's your rank.");

define("Q5","任务 5: 两个建造订单");
define("Q5_DESC","Build an iron mine and a clay pit. Of iron and clay one can never have enough.");
define("Q5_ORDER","Order:<\/p><ul><li>Extend one iron mine.<\/li><li>Extend one clay pit.<\/li><\/ul>");
define("Q5_RESP","As you noticed, building orders take rather long. The world of ". SERVER_NAME ." will continue to spin even if you are offline. Even in a few months there will be many new things for you to discover.<br \/><br \/>The best thing to do is occasionally checking your village and giving you subjects new tasks to do.");

define("Q6","任务 6: 消息");
define("Q6_DESC","You can talk to other players using the messaging system. I sent a message to you. Read it and come back here.<br \/><br \/>P.S. Don't forget: on the left the reports, on the right the messages.");
define("Q6_ORDER","Order:<\/p>Read your new message.");
define("Q6_RESP","You received it? Very good.<br \/><br \/>Here is some Gold. With Gold you can do several things, e.g. extend your   in the left hand menu.");
define("Q6_RESP1","-Account or increase your resource production.To do so click ");
define("Q6_RESP2","in the left hand menu.");
define("Q6_SUBJECT","Message From The Taskmaster");
define("Q6_MESSAGE","You are to be informed that a nice reward is waiting for you at the taskmaster.<br /><br />Hint: The message has been generated automatically. An answer is not necessary.");

define("Q7","任务 7: 每种资源地升到1级！");
define("Q7_DESC","Now we should increase your resource production a bit. Build an additional woodcutter, clay pit, iron mine and cropland to level 1.");
define("Q7_ORDER","Order:<\/p>Extend one more of each resource tile to level 1.");
define("Q7_RESP","Very good, great develop of resources production.");

define("Q8","任务 8: 庞大的军队！");
define("Q8_DESC","Now I've got a very special quest for you. I am hungry. Give me 200 crop!<br \/><br \/>In return I will try to organize a huge army to protect your village.");
define("Q8_ORDER","Order:<\/p>Send 200 crop to the taskmaster.");
define("Q8_BUTN","Send crop");
define("Q8_NOCROP","No Enough Crop!");

define("Q9","任务 9: 全部资源地升到1级！");
define("Q9_DESC","In Travian there is always something to do! While you are waiting for incoming the huge army, Now we should increase your resource production a bit. Extend all your resource tiles to level 1.");
define("Q9_ORDER","Order:<\/p>Extend all resource tiles to level 1.");
define("Q9_RESP","Very good, your resource production just thrives.<br \/><br \/>Soon we can start with constructing buildings in the village.");

define("Q10","任务 10: 和平鸽");
define("Q10_DESC","The first days after signing up you are protected against attacks by your fellow players. You can see how long this protection lasts by adding the code <b>[#0]<\/b> to your profile.");
define("Q10_ORDER","Order:<\/p>Write the code <b>[#0]<\/b> into your profile by adding it to one of the two description fields.");
define("Q10_RESP","Well done! Now everyone can see what a great warrior the world is approached by.");
define("Q10_REWARD","Your reward:<\/p>2 day Travian");

define("Q11","任务 11: 邻居！");
define("Q11_DESC","Around you, there are many different villages. One of them is named. ");
define("Q11_DESC1"," Click on 'map' in the header menu and look for that village. The name of your neighbours' villages can be seen when hovering your mouse over any of them.");
define("Q11_ORDER","Order:</p>Look for the coordinates of ");
define("Q11_ORDER1","and enter them here.");
define("Q11_RESP","Exactly, there ");
define("Q11_RESP1"," Village! As many resources as you reach this village. Well, almost as much ...");
define("Q11_BUTN","completar misi&oacute;n");

define("Q12","任务 12: 山洞");
define("Q12_DESC","It's getting time to erect a cranny. The world of <?php echo SERVER_NAME; ?> is dangerous.<br \/><br \/>Many players live by stealing other players' resources. Build a cranny to hide some of your resources from enemies.");
define("Q12_ORDER","Order:<\/p>Construct a Cranny.");
define("Q12_RESP","Well done, now it's way harder for your mean fellow players to plunder your village.<br \/><br \/>If under attack, your villagers will hide the resources in the Cranny all on their own.");

define("Q13","任务 13: 每种资源地升到等级2！");
define("Q13_DESC","In <?php echo SERVER_NAME; ?> there is always something to do! Extend one woodcutter, one clay pit, one iron mine and one cropland to level 2 each.");
define("Q13_ORDER","Order:<\/p>Extend one of each resource tile to level 2.");
define("Q13_RESP","Very good, your village grows and thrives!");

define("Q14","任务 14: 说明");
define("Q14_DESC","In the ingame instructions you can find short information texts about different buildings and types of units.<br \/><br \/>Click on 'instructions' at the left to find out how much lumber is required for the barracks.");
define("Q14_ORDER","Order:<\/p>Enter how much lumber barracks cost");
define("Q14_BUTN","complete task");
define("Q14_RESP","Exactly! Barracks cost 210 lumber.");

define("Q15","任务 15: 中心大楼");
define("Q15_DESC","Your master builders need a main building level 3 to erect important buildings such as the marketplace or barracks.");
define("Q15_ORDER","Order:<\/p>Extend your main building to level 3.");
define("Q15_RESP","Well done. The main building level 3 has been completed.<br><br>With this upgrade your master builders cannot only construct more types of buildings but also do so faster.");

define("Q16","任务 16: 排名提升！");
define("Q16_DESC","Look up your rank in the player statistics again and enjoy your progress.");
define("Q16_ORDER","Order:<\/p>Look for your rank in the statistics and enter it here.");
define("Q16_RESP","Well done! That's your current rank.");

define("Q17","任务 17: 战斗或生产");
define("Q17_DESC","Now you have to make a decision: Either trade peacefully or become a dreaded warrior.<br \/><br \/>For the marketplace you need a granary, for the barracks you need a rally point.");
define("Q17_BUTN","经济");
define("Q17_BUTN1","军事");

define("Q18","任务 18: 军事");
define("Q18_DESC","A brave decision. To be able to send troops you need a rally point.<br \/><br \/>The rally point must be built on a specific building site. The ");
define("Q18_DESC1"," building site.");
define("Q18_DESC2"," is located on the right side of the main building, slightly below it. The building site itself is curved.");
define("Q18_ORDER","Order:<\/p>Construct a rally point.");
define("Q18_RESP","Your rally point has been erected! A good move towards world domination!");

define("Q19","任务 19: 兵营");
define("Q19_DESC","Now you have a main building level 3 and a rally point. That means that all prerequisites for building barracks have been fulfilled.<br><br>You can use the barracks to train troops for fighting.");
define("Q19_ORDER","Order:<\/p>Construct barracks.");
define("Q19_RESP","Well done... The best instructors from the whole country have gathered to train your men\u2019s fighting skills to top form.");

define("Q20","任务 20: 训练");
define("Q20_DESC","Now that you have barracks you can start training troops. Train two ");
define("Q20_ORDER","Please train 2 ");
define("Q20_RESP","The foundation for your glorious army has been laid.<br \/><br \/>Before sending your army off to plunder you should check with the.");
define("Q20_RESP1","Combat Simulator");
define("Q20_RESP2","to see how many troops you need to successfully fight one rat without losses.");

define("Q21","任务 18: 经济");
define("Q21_DESC","Trade & Economy was your choice. Golden times await you for sure!");
define("Q21_ORDER","Order:<\/p>Construct a Granary.");
define("Q21_RESP","Well done! With the Granary you can store more wheat.");

define("Q22","任务 19: 仓库");
define("Q22_DESC","Not only Crop has to be saved. Other resources can go to waste as well if they are not stored correctly. Construct a Warehouse!");
define("Q22_ORDER","Order:<\/p>Construct Warehouse.");
define("Q22_RESP",";Well done, your Warehouse is complete...&rdquo;<\/i><br \/>Now you have fulfilled all prerequisites required to construct a Marketplace.");

define("Q23","任务 20: 市场");
define("Q23_DESC",";Construct a Marketplace so you can trade with your fellow players.");
define("Q23_ORDER","Order:<\/p>Please build a Marketplace.");
define("Q23_RESP",";The Marketplace has been completed. Now you can make offers of your own and accept foreign offers! When creating your own offers, you should think about offering what other players need most to get more profit.");

define("Q24","任务 21: 所有资源地升到等级2！");
define("Q24_DESC","Now we should increase your resource production a bit. Build an additional woodcutter, clay pit, iron mine and cropland to level 1.");
define("Q24_ORDER","Order:<\/p>Extend all resource tiles to level 2.");
define("Q24_RESP","Congratulations! Your village grows and thrives...");

define("Q28","任务 22: 公会");
define("Q28_DESC","Teamwork is important in Travian. Players who work together organise themselves in alliances. Get an invitation from an alliance in your region and join this alliance. Alternatively, you can found your own alliance. To do this, you need a level 3 embassy.");
define("Q28_ORDER","Order:<\/p>Join an alliance or found one on your own.");
define("Q28_RESP","Is good! Now you're in a union called");
define("Q28_RESP1",", and you're a member of their alliance with the faster you'll progress...");

define("Q29","任务 23: 中心大楼升到等级5");
define("Q29_DESC","To be able to build a palace or residence, you will need a main building at level 5.");
define("Q29_ORDER","Order:<\/p>Upgrade your main building to level 5.");
define("Q29_RESP","The main building is level 5 now and you can build palace or residence...");

define("Q30","任务 24: 粮仓升到等级3");
define("Q30_DESC","That you do not lose crop, you should upgrade your granary.");
define("Q30_ORDER","Order:<\/p>Upgrade your granary to level 3.");
define("Q30_RESP","Granary is level 3 now...");

define("Q31","任务 25: 仓库升到等级7");
define("Q31_DESC"," To make sure your resources won't overflow, you should upgrade your warehouse.");
define("Q31_ORDER","Order:<\/p>Upgrade your warehouse to level 7.");
define("Q31_RESP","Warehouse has upgraded to level 7...");

define("Q32","任务 26: 所有资源地升到等级5！");
define("Q32_DESC","You will always need more resources. Resource tiles are quite expensive but will always pay out in the long term.");
define("Q32_ORDER","Order:<\/p>Upgrade all resources tiles to level 5.");
define("Q32_RESP","All resources are at level 5, very good, your village grows and thrives!");

define("Q33","任务 27: 皇宫或行宫？");
define("Q33_DESC","To found a new village, you will need settlers. They can be trained in either a palace or a residence.");
define("Q33_ORDER","Order:<\/p>Build a palace or residence to level 10.");
define("Q33_RESP","had reached to level 10, you can now train settlers and found your second village. Notice the cultural points...");

define("Q34","任务 28: 建造3个拓荒者！");
define("Q34_DESC","To found a new village, you will need settlers. You can train them in the palace or residence.");
define("Q34_ORDER","Order:<\/p>Train 3 settlers.");
define("Q34_RESP","3 settlers were trained. To found new village you need at least");
define("Q34_RESP1","culture points...");

define("Q35","任务 29: 新的村庄！");
define("Q35_DESC","There are a lot of empty tiles on the map. Find one that suits you and found a new village");
define("Q35_ORDER","Order:<\/p>Found a new village.");
define("Q35_RESP","I am proud of you! Now you have two villages and have all the possibilities to build a mighty empire. I wish you luck with this.");

define("Q36","任务 30: 建造一个...");
define("Q36_DESC","Now that you have trained some soldiers, you should build a ");
define("Q36_DESC1"," too. It increases the base defence and your soldiers will receive a defensive bonus.");
define("Q36_ORDER","Order:<\/p>Build a ");
define("Q36_RESP","That's what I'm talking about. A ");
define("Q36_RESP1"," Very useful. It increases the defence of the troops in the village.");

define("Q37","任务");
define("Q37_DESC","完成所有任务！");

define("OPT3","Resource overview");
define("T","Your resource deliveries");
define("T1","Delivery");
define("T2","Delivery time");
define("T3","Status");
define("T4","fetch");
define("T5","fetched");
define("T6","on hold");
define("T7","1 day Travian ");
define("T8","2 days Travian ");
//======================================================//
//================ UNITS - DO NOT EDIT! ================//
//======================================================//
define("U0","英雄");

//ROMAN UNITS
define("U1","罗马步兵");
define("U2","禁卫兵");
define("U3","帝国兵");
define("U4","使者骑士");
define("U5","帝国骑士");
define("U6","将军骑士");
define("U7","冲撞车");
define("U8","火焰投石机");
define("U9","参议员");
define("U10","拓荒者");

//TEUTON UNITS
define("U11","棍棒兵");
define("U12","矛兵");
define("U13","斧头兵");
define("U14","侦察兵");
define("U15","圣骑士");
define("U16","日耳曼骑士");
define("U17","冲撞车");
define("U18","投石机");
define("U19","司机官");
define("U20","拓荒者");

//GAUL UNITS
define("U21","方阵兵");
define("U22","剑士");
define("U23","探路者");
define("U24","雷法师");
define("U25","德鲁伊骑兵");
define("U26","海顿圣骑");
define("U27","冲撞车");
define("U28","投石机");
define("U29","族长");
define("U30","拓荒者");
define("U99","陷阱机");

//NATURE UNITS
define("U31","老鼠");
define("U32","蜘蛛");
define("U33","蛇");
define("U34","蝙蝠");
define("U35","野猪");
define("U36","狼");
define("U37","熊");
define("U38","鳄鱼");
define("U39","老虎");
define("U40","大象");

//NATARS UNITS
define("U41","长枪兵");
define("U42","刺锤武士");
define("U43","近卫兵");
define("U44","狩猎鸟");
define("U45","斧骑兵");
define("U46","纳塔骑士");
define("U47","战象");
define("U48","投石车");
define("U49","纳塔皇帝");
define("U50","拓荒者");

//MONSTER UNITS
define("U51","Monster Peon");
define("U52","Monster Hunter");
define("U53","Monster Warrior");
define("U54","Ghost");
define("U55","Monster Steed");
define("U56","Monster War Steed");
define("U57","Monster Ram");
define("U58","Monster Catapult");
define("U59","Monster Chief");
define("U60","Monster Settler");

// RESOURCES
define("R1","木材");
define("R2","砖块");
define("R3","钢铁");
define("R4","粮食");

//INDEX.php
define("LOGIN","登录");
define("PLAYERS","玩家");
define("ACTIVE","活跃");
define("ONLINE","在线");
define("TUTORIAL","Tutorial");
define("PLAYER_STATISTICS","Player statistics");
define("TOTAL_PLAYERS","".PLAYERS."总数");
define("ACTIVE_PLAYERS","活跃玩家");
define("ONLINE_PLAYERS","在线".PLAYERS);
define("MP_STRATEGY_GAME","".SERVER_NAME." - the multiplayer strategy game");
define("WHAT_IS","".SERVER_NAME." is one of the most popular browser games in the world. As a player in ".SERVER_NAME.", you will build your own empire, recruit a mighty army, and fight with your allies for game world hegemony.");
define("REGISTER_FOR_FREE","Register here for free!");
define("LATEST_GAME_WORLD","Latest game world");
define("LATEST_GAME_WORLD2","Register on the latest<br/>game world and enjoy<br/>the advantages of<br/>being one of the<br/>first players.");
define("PLAY_NOW","现在开始玩".SERVER_NAME);
define("LEARN_MORE","学习更多 <br/>关于 ".SERVER_NAME."!");
define("LEARN_MORE2","Now with a revolutionised<br>server system, completely new<br>graphics <br>This clone is The Shiz!");
define("COMUNITY","Community");
define("BECOME_COMUNITY","Become part of our community now!");
define("BECOME_COMUNITY2","Become a part of one of<br>the biggest gaming<br>communities in the<br>world.");
define("NEWS","News");
define("SCREENSHOTS","Screenshots");
define("LEARN1","Upgrade your fields and mines to increase your resource production. You will need resources to construct buildings and train soldiers.");
define("LEARN2","Construct and expand the buildings in your village. Buildings improve your overall infrastructure, increase your resource production and allow you to research, train and upgrade your troops.");
define("LEARN3","View and interact with your surroundings. You can make new friends or new enemies, make use of the nearby oases and observe as your empire grows and becomes stronger.");
define("LEARN4","Follow your improvement and success and compare yourself to other players. Look at the Top 10 rankings and fight to win a weekly medal.");
define("LEARN5","Receive detailed reports about your adventures, trades and battles. Don't forget to check the brand new reports about the happenings taking place in your surroundings.");
define("LEARN6","Exchange information and conduct diplomacy with other players. Always remember that communication is the key to winning new friends and solving old conflicts.");
define("LOGIN_TO","Log in to ". SERVER_NAME);
define("REGIN_TO","Register in ". SERVER_NAME);
define("P_ONLINE","Players online: ");
define("P_TOTAL","Players in total: ");
define("CHOOSE","Please choose a server.");
define("STARTED"," The server started ". round((time()-COMMENCE)/86400) ." days ago.");

//ANMELDEN.php
define("NICKNAME","昵称");
define("EMAIL","邮箱地址");
define("PASSWORD","密码");
define("ROMANS","罗马人");
define("TEUTONS","日耳曼人");
define("GAULS","高卢人");
define("NW","西北方");
define("NE","东北方");
define("SW","西南方");
define("SE","东南方");
define("RANDOM","随机");
define("ACCEPT_RULES"," 我接受本游戏 的 游戏规定 和 条款.");
define("ONE_PER_SERVER","每个服务器，每个玩家只能拥有一个帐户.");
define("BEFORE_REGISTER","在你创建帐号之前請你务必阅读 <a href='../anleitung.php' target='_blank'>游戏说明</a> 了解 TravianZ 3個种族的优点和缺点");
define("BUILDING_UPGRADING","建筑:");
define("HOURS","小时");


//ATTACKS ETC.
define("TROOP_MOVEMENTS","军队移动:");
define("ARRIVING_REINF_TROOPS","Arriving reinforcing troops");
define("ARRIVING_REINF_TROOPS_SHORT","Reinf.");
define("OWN_ATTACKING_TROOPS","Own attacking troops");
define("ATTACK","攻击");
define("OWN_REINFORCING_TROOPS","Own reinforcing troops");
define("TROOPS_DORF","军队:");
define("NEWVILLAGE","New vill.");
define("FOUNDNEWVILLAGE","Founding New village");
define("UNDERATTACK","The village is under attack");
define("OASISATTACK","The Oasis is under attack");
define("OASISATTACKS","Oasis Att.");
define("RETURNFROM","Return from");
define("REINFORCEMENTFOR","Reinforcement to");
define("ATTACK_ON","Attack to");
define("RAID_ON","Raid to");
define("SCOUTING","Scouting");
define("PRISONERS","Prisioners");
define("PRISONERSIN","Prisioners in");
define("PRISONERSFROM","Prisioners from");
define("TROOPS","军队");
define("TROOPSFROM","Troops");
define("BOUNTY","Bounty");
define("ARRIVAL","Arrival");
define("INCOMING_TROOPS","Incoming Troops");
define("TROOPS_ON_THEIR_WAY","Troops on their way");
define("OWN_TROOPS","Own troops");
define("ON","on");
define("AT","at");
define("UPKEEP","粮食消耗");
define("SEND_BACK","Send back");
define("TROOPS_IN_THE_VILLAGE","Troops in the village");
define("TROOPS_IN_OTHER_VILLAGE","Troops in other village");
define("TROOPS_IN_OASIS","Troops in oasis");
define("KILL","Kill");
define("FROM","From");
define("SEND_TROOPS","Send troops");
define("TASKMASTER","Taskmaster");
define("VILLAGE_OF_THE_ELDERS_TROOPS","village of the elders troops");

//map
define("DETAIL","Details");
define("ABANDVALLEY","Abandoned valley");
define("UNOCCUOASIS","未被占领的绿洲");
define("OCCUOASIS","被占领的绿洲");
define("THERENOINFO","There is no<br>information available.");
define("LANDDIST","Land distribution");
define("TRIBE","Tribe");
define("ALLIANCE","Alliance");
define("OWNER","Owner");
define("POP","Population");
define("REPORT","Report");
define("OPTION","Options");
define("CENTREMAP","Centre map");
define("FNEWVILLAGE","Found new village");
define("CULTUREPOINT","culture points");
define("BUILDRALLY","build a rally point");
define("SETTLERSAVAIL","settlers available");
define("BEGINPRO","beginners protection");
define("SENDMERC","Send merchant(s)");
define("BAN","Player is banned");
define("BUILDMARKET","Build marketplace");
define("PERHOUR","每小时");
define("BONUS","Bonus");
define("MAP","Map");
define("CROPFINDER","村庄查找器");
define("NORTH","North");
define("EAST","East");
define("SOUTH","South");
define("WEST","West");

//LOGIN.php
define("COOKIES","如果你要和其他人共享这台电脑，请先注销以确保你的帐号安全。");
define("NAME","名称");
define("PW_FORGOTTEN","忘记密码？");
define("PW_REQUEST","你可以请求一个新密码发送到你的注册邮箱。");
define("PW_GENERATE","生成一个新密码。");
define("EMAIL_NOT_VERIFIED","邮箱地址不正确");
define("EMAIL_FOLLOW","Follow this link to activate your account.");
define("VERIFY_EMAIL","验证邮箱.");
define("SERVER_STARTS_IN","Server will start in: ");
define("START_NOW","现在开始");


//404.php
define("NOTHING_HERE","Nothing here!");
define("WE_LOOKED","We looked 404 times already but can't find anything");

//TIME RELATED
define("CALCULATED","产生在");
define("SERVER_TIME","服务器时间：");

//MASSMESSAGE.php
define("MASS","Message Content");
define("MASS_SUBJECT","主旨:");
define("MASS_COLOR","Message color:");
define("MASS_REQUIRED","All fields required");
define("MASS_UNITS","Images (units):");
define("MASS_SHOWHIDE","Show/Hide");
define("MASS_READ","Read this: after adding smilie, you have to add left or right after number otherwise image will won't work");
define("MASS_CONFIRM","Confirmation");
define("MASS_REALLY","Do you really want to send MassIGM?");
define("MASS_ABORT","Aborting right now");
define("MASS_SENT","Mass IGM was sent");

//BUILDINGS

define("WOODCUTTER","伐木场");
define("CLAYPIT","泥坑");
define("IRONMINE","铁矿场");
define("CROPLAND","农场");

define("SAWMILL","锯木厂");
define("SAWMILL_DESC","锯木厂是后期用来增加木材产量的。每一级锯木厂会将木材产量提升5%，总共有5級，即最多有25%木材产量提升。");
define("CURRENT_WOOD_BONUS","当前木材加成：");
define("WOOD_BONUS_LEVEL","木材加成于等级");
define("MAX_LEVEL","建议物已为最大等级");
define("PERCENT","%");

define("BRICKYARD","砖厂");
define("CURRENT_CLAY_BONUS","当前砖块加成：");
define("CLAY_BONUS_LEVEL","砖块加成于等级");
define("BRICKYARD_DESC","砖厂是生产砖块的地方。每升一级能增加5%的产量，一共5级，也就是最多能增加25%的产量。");

define("IRONFOUNDRY","铸铁铸造厂");
define("CURRENT_IRON_BONUS","当前钢铁加成：");
define("IRON_BONUS_LEVEL","钢铁加成于等级");
define("IRONFOUNDRY_DESC","铸铁铸造厂是用来增加钢铁产量的。每升一级，钢铁产量將提升5%，总共有5级，即最多能提升25%的钢铁产量。");

define("GRAINMILL","面粉厂");
define("CURRENT_CROP_BONUS","当前粮食加成：");
define("CROP_BONUS_LEVEL","粮食加成于等级");
define("GRAINMILL_DESC","面粉厂是生产面粉的地方。每升一级面粉厂，产量將提升5%，总共有5级，即最多能提升25%的产量。");

define("BAKERY","面包店");
define("BAKERY_DESC","面包店將面粉厂生产的面粉烤成面包。与面粉厂一起最高可提升50%的粮食产量。");

define("WAREHOUSE","仓库");
define("CURRENT_CAPACITY","当前容量：");
define("CAPACITY_LEVEL","容量于等级");
define("RESOURCE_UNITS","Resource units");
define("WAREHOUSE_DESC","The resources wood, clay and iron are stored in your Warehouse. By increasing its level you increase your Warehouse's capacity.");

define("GRANARY","粮仓");
define("CROP_UNITS","Crop units");
define("GRANARY_DESC","Crop produced by your farms is stored in the Granary. By increasing its level you increase the Granary's capacity.");

define("BLACKSMITH","铁匠铺");
define("ACTION","Action");
define("UPGRADE","升级");
define("UPGRADE_IN_PROGRESS","Upgrade in<br>progress");
define("UPGRADE_BLACKSMITH","Upgrade<br>blacksmith");
define("UPGRADES_COMMENCE_BLACKSMITH","Upgrades can commence when blacksmith is completed.");
define("MAXIMUM_LEVEL","Maximum<br>level");
define("EXPAND_WAREHOUSE","Expand<br>warehouse");
define("EXPAND_GRANARY","Expand<br>granary");
define("ENOUGH_RESOURCES","足够的资源");
define("CROP_NEGATIVE ","Crop production is negative so you will never reach the required resources");
define("TOO_FEW_RESOURCES","Too few<br>resources");
define("UPGRADING","Upgrading");
define("DURATION","Duration");
define("COMPLETE","Complete");
define("BLACKSMITH_DESC","In the blacksmith's melting furnaces your warriors' weapons are enhanced. By increasing its level you can order the fabrication of even better weapons.");

define("ARMOURY","军械库");
define("UPGRADE_ARMOURY","Upgrade<br>Armoury");
define("UPGRADES_COMMENCE_ARMOURY","Upgrades can commence when armoury is completed.");
define("ARMOURY_DESC","In the armoury's melting furnaces your warriors' armour is enhanced. By increasing its level you can order the fabrication of even better armour.");

define("TOURNAMENTSQUARE","竞技场");
define("CURRENT_SPEED","当前速度加成：");
define("SPEED_LEVEL","速度加成于等级");
define("TOURNAMENTSQUARE_DESC","在竞技场你能训练军队的速度。增加士兵行军速度，但效果只有在距离".TS_THRESHOLD."格后生效。");

define("MAINBUILDING","中心大楼");
define("CURRENT_CONSTRUCTION_TIME","当前建造时间：");
define("CONSTRUCTION_TIME_LEVEL","建造时间于等级");
define("DEMOLITION_BUILDING","拆除建筑：</h2><p>如果某个建筑没有用了，你可以使用此功能进行拆除。</p>");
define("DEMOLISH","拆除");
define("DEMOLITION_OF","Demolition of ");
define("MAINBUILDING_DESC","In the main building the village's master builders live. The higher its level the faster your master builders complete the construction of new buildings.");

define("RALLYPOINT","集结点");
define("OVERVIEW","概况");
define("REINFORCEMENT","Reinforcement");
define("EVASION_SETTINGS","evasion settings");
define("SEND_TROOPS_AWAY_MAX","Send troops away a maximun of");
define("TIMES","times");
define("PER_EVASION","per evasion");
define("RALLYPOINT_DESC","Your village's troops meet here. From here you can send them out to conquer, raid or reinforce other villages.");

define("MARKETPLACE","市场");
define("MERCHANT","商人");
define("OR_","or");
define("GO","go");
define("UNITS_OF_RESOURCE","units of resource");
define("MERCHANT_CARRY","Each merchant can carry");
define("MERCHANT_COMING","Merchants coming");
define("TRANSPORT_FROM","Transport from");
define("ARRIVAL_IN","Arrival in");
define("NO_COORDINATES_SELECTED","No Coordinates selected");
define("CANNOT_SEND_RESOURCES","You cannot send resources to the same village");
define("BANNED_CANNOT_SEND_RESOURCES","Player is Banned. You cannot send resources to him");
define("RESOURCES_NO_SELECTED","Resources not selected");
define("ENTER_COORDINATES","Enter coordinates or village name");
define("TOO_FEW_MERCHANTS","Too few merchants");
define("OWN_MERCHANTS_ONWAY","Own merchants on the way");
define("MERCHANTS_RETURNING","Merchants returning");
define("TRANSPORT_TO","Transport to");
define("I_AN_SEARCHING","I'm searching");
define("I_AN_OFFERING","I'm offering");
define("OFFERS_MARKETPLACE","Offers at the marketplace");
define("OFFERED_TO_ME","Offered<br>to me");
define("WANTED_TO_ME","Wanted<br>from me");
define("NOT_ENOUGH_MERCHANTS","Not Enough Merchant");
define("ACCEP_OFFER","Accept offer");
define("NO_AVALIBLE_OFFERS","There are no avaliable offers on the market");
define("SEARCHING","Searching");
define("OFFERING","Offering");
define("MAX_TIME_TRANSPORT","max. time of transport");
define("OWN_ALLIANCE_ONLY","own alliance only");
define("INVALID_OFFER","Invalid offer");
define("OFFER","Offer");
define("SEARCH","Search");
define("OWN_OFFERS","Own offers");
define("ALL","All");
define("NPC_TRADE","NPC Trade");
define("SUM","Sum");
define("REST","Rest");
define("TRADE_RESOURCES","Trade resources at (step 2 of 2");
define("DISTRIBUTE_RESOURCES","Distribute resources at (step 1 of 2)");
define("OF","of");
define("NPC_COMPLETED","NPC completed");
define("BACK_BUILDING","Back to building");
define("YOU_CAN_NAT_NPC_WW","You can't use NPC trade in WW village.");
define("NPC_TRADING","NPC trading");
define("SEND_RESOURCES","运送资源");
define("BUY","购买");
define("TRADE_ROUTES","trade routes");
define("DESCRIPTION","Description");
define("TIME_LEFT","Time left");
define("START","Start");
define("NO_TRADE_ROUTES","No active trade routes");
define("TRADE_ROUTE_TO","Trade route to");
define("CHECKED","checked");
define("DAYS","days");
define("EXTEND","extend");
define("EDIT","edit");
define("EXTEND_TRADE_ROUTES","Extend the trade route by 7 days for");
define("CREATE_TRADE_ROUTES","Create new trade route");
define("DELIVERIES","deliveries");
define("START_TIME_TRADE","start time");
define("CREATE_TRADE_ROUTE","Create trade route");
define("TARGET_VILLAGE","target village");
define("EDIT_TRADE_ROUTES","Edit trade route");
define("TRADE_ROUTES_DESC","Trade route allows you to set up routes for your merchant that he will walk every day at a certain hour. <br /><br /> Standard this holds on for 7 days, but you can extend it with 7 days for the cost of");
define("NPC_TRADE_DESC","With the NPC merchant you can distribute the resources in your warehouse as you desire. <br /><br /> The first line shows the current stock. In the second line you can choose another distribution. The third line shows the difference between the old and new stock.");
define("MARKETPLACE_DESC","At the Marketplace you can trade resources with other players. The higher its level, the more resources can be transported at the same time.");

define("EMBASSY","大使馆");
define("ALLIANCE","公会");
define("TAG","标签");
define("TO_THE_ALLIANCE","进入公会");
define("JOIN_ALLIANCE","加入公会");
define("REFUSE","拒绝");
define("ACCEPT","接受");
define("NO_INVITATIONS","暂时还没有公会邀请你入会。");
define("NO_CREATE_ALLIANCE","Banned player can't create an alliance.");
define("FOUND_ALLIANCE","搜索公会");
define("EMBASSY_DESC","The embassy is a place for diplomats. The higher its level the more options the king gains.");

define("BARRACKS","兵营");
define("QUANTITY","数量");
define("MAX","最大");
define("TRAINING","训练");
define("FINISHED","完成");
define("UNIT_FINISHED","下一个单位训练完成于");
define("AVAILABLE","拥有");
define("TRAINING_COMMENCE_BARRACKS","Training can commence when barracks are completed.");
define("BARRACKS_DESC","All foot soldiers are trained in the barracks. The higher the level of the barracks, the faster the troops are trained.");

define("STABLE","马厩");
define("AVAILABLE_ACADEMY","No units available. Research at academy");
define("TRAINING_COMMENCE_STABLE","Training can commence when stable are completed.");
define("STABLE_DESC","马厩是一个训练骑兵的场所，每一名強悍的骑兵都经由马厩训练出來的。马厩等级越高，训练骑兵的速度便越快。");

define("WORKSHOP","工厂");
define("TRAINING_COMMENCE_WORKSHOP","Training can commence when workshop are completed.");
define("WORKSHOP_DESC","工厂是生产冲撞车和投石器的地方，工厂等级越高，生产速度便越快。");

define("ACADEMY","研究院");
define("RESEARCH_AVAILABLE","There are no researches available");
define("RESEARCH_COMMENCE_ACADEMY","Research can commence when academy is completed.");
define("RESEARCH","研究");
define("EXPAND_WAREHOUSE1","Expand warehouse");
define("EXPAND_GRANARY1","Expand granary");
define("RESEARCH_IN_PROGRESS","Research in<br>progress");
define("RESEARCHING","正在研究");
define("PREREQUISITES","前提条件");
define("SHOW_MORE","显示更多");
define("HIDE_MORE","隐藏更多");
define("ACADEMY_DESC","研究院是用来研究新的兵种。研究院等级越高，越高级的兵种就可以被研究。");

define("CRANNY","山洞");
define("CURRENT_HIDDEN_UNITS","Currently hidden units per resource:");
define("HIDDEN_UNITS_LEVEL","Hidden units per resource at level");
define("UNITS","units");
define("CRANNY_DESC","山洞可以帮你隐藏部分资源以免被掠走。若还有需要，可以再加建一個新的山洞。日耳曼人可以使敌人的山洞只有80%的作用，而高卢人有两倍大的山洞。");

define("TOWNHALL","市政厅");
define("CELEBRATIONS_COMMENCE_TOWNHALL","Celebrations can commence when the town hall is completed.");
define("GREAT_CELEBRATIONS","Great celebration");
define("CULTURE_POINTS","culture points");
define("HOLD","hold");
define("CELEBRATIONS_IN_PROGRESS","Celebration</br>in progress");
define("CELEBRATIONS","Celebrations");
define("TOWNHALL_DESC","在市政厅可以举办各种活动，以此来增加文明点，等级越高，派对所需時間越短。建立新村或征服村落需要文明點。只要興建或增加建築物等級，文明點便會產生。通過舉辦派對，文明點可以在短時間內很快提高。
You can hold pompous celebrations in the Town Hall. Such a celebration increases your culture points. Building up your Town Hall to a higher level will decrease the length of the celebration.");

define("RESIDENCE","行宫");
define("CAPITAL","这是你的首都");
define("RESIDENCE_TRAIN_DESC","In order to found a new village you need a level 10 or 20 residence and 3 settlers. In order to conquer a new village you need a level 10 or 20 residence and a senator, chief or chieftain.");
define("PRODUCTION_POINTS","Production of this village:");
define("PRODUCTION_ALL_POINTS","Production of all villages:");
define("POINTS_DAY","每日获得的文明点 ");
define("VILLAGES_PRODUCED","Your villages have produced");
define("POINTS_NEED","points in total. To found or conquer a new village you need");
define("POINTS","点数");
define("VILLAGE","村庄");
define("PLAYER","Player");
define("INHABITANTS","Inhabitants");
define("COORDINATES","Coordinates");
define("EXPANSION","Expansion");
define("TRAIN","训练");
define("DATE","Date");
define("CONQUERED_BY_VILLAGE","Villages founded or conquered by this village");
define("NONE_CONQUERED_BY_VILLAGE","No other village has been founded or conquered by this village yet.");
define("RESIDENCE_CULTURE_DESC","In order to extend your empire you need culture points. These culture points increase in the course of time and do so faster as your building levels increase.");
define("RESIDENCE_LOYALTY_DESC","By attacking with senators, chiefs or chieftains a village's loyalty can be brought down. If it reaches zero, the village joins the realm of the attacker. The loyalty of this village is currently at ");
define("RESIDENCE_DESC","The residence is a small palace, where the king or queen lives when (s)he visits the village. The residence protects the village against enemies who want to conquer it.");

define("PALACE","皇宫");
define("PALACE_CONSTRUCTION","Palace under construction");
define("PALACE_TRAIN_DESC","In order to found a new village you need a level 10, 15 or 20 palace and 3 settlers. In order to conquer a new village you need a level 10, 15 or 20 palace and a senator, chief or chieftain.");
define("CHANGE_CAPITAL","迁都");
define("SECURITY_CHANGE_CAPITAL","你确实要迁都吗？<br /><b>确定后不能撤消</b><br />保险起见，请先输入密码验证：<br />");
define("PALACE_DESC","The king or queen of the empire lives in the palace. Only one palace can exist in your realm at a time. You need a palace in order to proclaim a village to be your capital.");

define("TREASURY","宝库");
define("ARTIFACTS_AREA","你所在地区的文物");
define("NO_ARTIFACTS_AREA","这个地区没有文物");
define("OWN_ARTIFACTS","自己的文物");
define("CONQUERED","征服");
define("DISTANCE","距离");
define("EFFECT","效果");
define("ACCOUNT","帐户");
define("SMALL_ARTIFACTS","小型文物");
define("LARGE_ARTIFACTS","大型文物");
define("NO_ARTIFACTS","这里没有文物。");
define("ANY_ARTIFACTS","你没有自己的文物。");
define("OWNER","主人");
define("AREA_EFFECT","影响区域");
define("BONUS","加成");
define("REQUIRED_LEVEL","等级要求");
define("TIME_CONQUER","征服时间");
define("TIME_ACTIVATION","激活时间");
define("FORMER_OWNER","前主人");
define("BUILDING_STRONGER","提高建筑物强度：");
define("BUILDING_WEAKER","降低建筑物强度：");
define("TROOPS_FASTER","提高行军速度：");
define("TROOPS_SLOWEST","降低行军速度：");
define("SPIES_INCREASE","提高侦察能力：");
define("SPIES_DECRESE","降低侦察能力：");
define("CONSUME_LESS","降低军队粮食消耗：");
define("CONSUME_HIGH","提高军队粮食消耗：");
define("TROOPS_MAKE_FASTER","提高行军速度：");
define("TROOPS_MAKE_SLOWEST","降低行军速度：");
define("YOU_CONSTRUCT","你可以建造 ");
define("CRANNY_INCREASED","提高山洞容量：");
define("CRANNY_DECRESE","降低山洞容量：");
define("TREASURY_DESC","你的所有财富都存放于宝库。宝库有一个存放文物的空间。在普通服，你夺取一个文物需要24小时，而3倍服只需12小时");

define("TRADEOFFICE","贸易办事处");
define("CURRENT_MERCHANT","当前商人负重:");
define("MERCHANT_LEVEL","商人负重于等级：");
define("TRADEOFFICE_DESC","贸易办事处为商人配备马匹以提高他们的负重。办事处等级越高，商人的负重量越大。");

define("GREATBARRACKS","大兵营");
define("TRAINING_COMMENCE_GREATBARRACKS","当大兵营建造完成后训练才会开始");
define("GREATBARRACKS_DESC","大兵营可以训练步兵。等级越高，训练速度越快。");

define("GREATSTABLE","Great Stable");
define("TRAINING_COMMENCE_GREATSTABLE","Training can commence when great stables are completed.");
define("GREATSTABLE_DESC","Cavalry can be trained in the great stable. The higher its level the faster the troops are trained.");

define("CITYWALL","城墙");
define("DEFENCE_NOW","当前防御加成：");
define("DEFENCE_LEVEL","防御加成于等级");
define("CITYWALL_DESC","城墙为村落提供防御力，保护村民抵挡外来的攻击。城墙等级越高，越能夠有效得防御入侵的敌方部落。");

define("EARTHWALL","Earth Wall");
define("EARTHWALL_DESC","By building a Earth Wall you can protect your village against the barbarian hordes of your enemies. The higher the wall's level, the higher the bonus given to your forces' defence.");

define("PALISADE","Palisade");
define("PALISADE_DESC","By building a Palisade you can protect your village against the barbarian hordes of your enemies. The higher the wall's level, the higher the bonus given to your forces' defence.");

define("STONEMASON","Stonemason's Lodge");
define("CURRENT_STABILITY","Current stability bonus:");
define("STABILITY_LEVEL","Stability bonus at level");
define("STONEMASON_DESC","The stonemason's lodge is an expert at cutting stone. The further the building is extended the higher the stability of the village's buildings.");

define("BREWERY","Brewery");
define("CURRENT_BONUS","Current bonus:");
define("BONUS_LEVEL","Bonus at level");
define("BREWERY_DESC","Tasty mead is brewed in the Brewery and later quaffed by the soldiers during the celebrations.");

define("TRAPPER","Trapper");
define("CURRENT_TRAPS","Currect maximum traps to train:");
define("TRAPS_LEVEL","Maximum traps to train at level");
define("TRAPS","Traps");
define("TRAP","Trap");
define("CURRENT_HAVE","Your currently have");
define("WHICH_OCCUPIED","of which are occupied.");
define("TRAINING_COMMENCE_TRAPPER","Training can commence when trapper are completed.");
define("TRAPPER_DESC","The trapper protects your village with well hidden traps. This means that unwary enemies can be imprisoned and won't be able to harm your village anymore.");

define("HEROSMANSION","英雄园");
define("HERO_READY","Hero will be ready in ");
define("NAME_CHANGED","英雄名字变更成功");
define("NOT_UNITS","Not available units");
define("NOT","Not ");
define("TRAIN_HERO","训练新英雄");
define("REVIVE","Revive");
define("OASES","Oases");
define("DELETE","删除");
define("RESOURCES","资源");
define("OFFENCE","攻击");
define("DEFENCE","防御");
define("OFF_BONUS","攻击加成");
define("DEF_BONUS","防御加成");
define("REGENERATION","恢复");
define("DAY","天");
define("EXPERIENCE","经验值");
define("YOU_CAN","你可以");
define("RESET","重置");
define("YOUR_POINT_UNTIL"," your points until you are level ");
define("OR_LOWER"," or lower!");
define("YOUR_HERO_HAS","你的英雄拥有 ");
define("OF_HIT_POINTS","战斗点数");
define("ERROR_NAME_SHORT","错误：名字太短了");
define("HEROSMANSION_DESC","In the Hero's mansion you can train your own hero and at level 10, 15 and 20 you can conquer oases with Hero in the immediate vicinity.");

define("GREATWAREHOUSE","Great Warehouse");
define("GREATWAREHOUSE_DESC","Wood, clay and iron are stored in the warehouse. The great warehouse offers you more space and keeps your goods drier and safer than the normal one.");

define("GREATGRANARY","Great Granary");
define("GREATGRANARY_DESC","Crop produced by your farms is stored in the granary. The great granary offers you more space and keeps your crops drier and safer than the normal one.");

define("WONDER","世界奇迹");
define("WORLD_WONDER","World Wonder");
define("WONDER_DESC","The World Wonder (otherwise known as a Wonder of the World) is as wonderful as it sounds. This building is built in order to win the server. Each level of the World Wonder costs hundreds of thousands (even millions) of resources to build.");
define("WORLD_WONDER_CHANGE_NAME","你需要将世界奇迹升到1级才能改名。");
define("WORLD_WONDER_NAME","世界奇迹名字");
define("WORLD_WONDER_NOTCHANGE_NAME","你的世界奇迹在10级以后不能改名");
define("WORLD_WONDER_NAME_CHANGED","名字更改成功");

define("HORSEDRINKING","Horse Drinking Trough");
define("HORSEDRINKING_DESC","The horse drinking trough of the Romans decreases the training time of cavalry and the upkeep of these troops as well.");

define("GREATWORKSHOP","大工厂");
define("TRAINING_COMMENCE_GREATWORKSHOP","当大工厂建造完成后训练才会开始");
define("GREATWORKSHOP_DESC","工厂是生产冲撞车和投石器的地方，工厂等级越高，生产速度便越快。");

define("BUILDING_MAX_LEVEL_UNDER","Building max level under construction");
define("BUILDING_BEING_DEMOLISHED","目前正在拆除的建筑");
define("COSTS_UPGRADING_LEVEL","花费</b> 升到等级");
define("WORKERS_ALREADY_WORK","工人们已经在工作了");
define("CONSTRUCTING_MASTER_BUILDER","Constructing with master builder ");
define("COSTS","花费");
define("GOLD","Gold");
define("WORKERS_ALREADY_WORK_WAITING","工人们已经在工作了（等待队列）");
define("ENOUGH_FOOD_EXPAND_CROPLAND","食物不足，请升级你的农场");
define("UPGRADE_WAREHOUSE","升级仓库");
define("UPGRADE_GRANARY","升级粮仓");
define("YOUR_CROP_NEGATIVE","你的粮食产量为负，你将无法获得粮食资源。");
define("UPGRADE_LEVEL","升到等级 ");
define("WAITING","（等待队列）");
define("NEED_WWCONSTRUCTION_PLAN","Need WW construction plan");
define("NEED_MORE_WWCONSTRUCTION_PLAN","Need more WW construction plan");
define("CONSTRUCT_NEW_BUILDING","建造新建筑");
define("SHOWSOON_AVAILABLE_BUILDINGS","显示即将可建造的建筑");
define("HIDESOON_AVAILABLE_BUILDINGS","隐藏即将可建造的建筑");



//artefact
define("ARCHITECTS_DESC","All buildings in the area of effect are stronger. This means that you will need more catapults to damage buildings protected by this artifacts powers.");
define("ARCHITECTS_SMALL","The architects slight secret");
define("ARCHITECTS_SMALLVILLAGE","Diamond Chisel");
define("ARCHITECTS_LARGE","The architects great secret");
define("ARCHITECTS_LARGEVILLAGE","Giant Marble Hammer");
define("ARCHITECTS_UNIQUE","The architects unique secret");
define("ARCHITECTS_UNIQUEVILLAGE","Hemons Scrolls");
define("HASTE_DESC","All troops in the area of effect move faster.");
define("HASTE_SMALL","The slight titan boots");
define("HASTE_SMALLVILLAGE","Opal Horseshoe");
define("HASTE_LARGE","The great titan boots");
define("HASTE_LARGEVILLAGE","Golden Chariot");
define("HASTE_UNIQUE","The unique titan boots");
define("HASTE_UNIQUEVILLAGE","Pheidippides Sandals");
define("EYESIGHT_DESC","All spies (Scouts, Pathfinders, and Equites Legati) increase their spying ability. In addition, with all versions of this artifact you can see the incoming TYPE of troops but not how many there are.");
define("EYESIGHT_SMALL","The eagles slight eyes");
define("EYESIGHT_SMALLVILLAGE","Tale of a Rat");
define("EYESIGHT_LARGE","The eagles great eyes");
define("EYESIGHT_LARGEVILLAGE","Generals Letter");
define("EYESIGHT_UNIQUE","The eagles unique eyes");
define("EYESIGHT_UNIQUEVILLAGE","Diary of Sun Tzu");
define("DIET_DESC","All troops in the artifacts range consume less wheat, making it possible to maintain a larger army.");
define("DIET_SMALL","Slight diet control");
define("DIET_SMALLVILLAGE","Silver Platter");
define("DIET_LARGE","Great diet control");
define("DIET_LARGEVILLAGE","Sacred Hunting Bow");
define("DIET_UNIQUE","Unique diet control");
define("DIET_UNIQUEVILLAGE","King Arthurs Chalice");
define("ACADEMIC_DESC","Troops are built a certain percentage faster within the scope of the artifact.");
define("ACADEMIC_SMALL","The trainers slight talent");
define("ACADEMIC_SMALLVILLAGE","Scribed Soldiers Oath");
define("ACADEMIC_LARGE","The trainers great talent");
define("ACADEMIC_LARGEVILLAGE","Declaration of War");
define("ACADEMIC_UNIQUE","The trainers unique talent");
define("ACADEMIC_UNIQUEVILLAGE","Memoirs of Alexander the Great");
define("STORAGE_DESC","With this building plan you are able to build the Great Granary or Great Warehouse in the Village with the artifact, or the whole account depending on the artifact. As long as you posses that artifact you are able to build and enlarge those buildings.");
define("STORAGE_SMALL","Slight storage masterplan");
define("STORAGE_SMALLVILLAGE","Builders Sketch");
define("STORAGE_LARGE","Great storage masterplan");
define("STORAGE_LARGEVILLAGE","Babylonian Tablet");
define("CONFUSION_DESC","Cranny capacity is increased by a certain amount for each type of artifact. Catapults can only shoot random on villages within this artifacts power. Exceptions are the WW which can always be targeted and the treasure chamber which can always be targeted, except with the unique artifact. When aiming at a resource field only random resource fields can be hit, when aiming at a building only random buildings can be hit.");
define("CONFUSION_SMALL","Rivals slight confusion");
define("CONFUSION_SMALLVILLAGE","Map of the Hidden Caverns");
define("CONFUSION_LARGE","Rivals great confusion");
define("CONFUSION_LARGEVILLAGE","Bottomless Satchel");
define("CONFUSION_UNIQUE","Rivals unique confusion");
define("CONFUSION_UNIQUEVILLAGE","Trojan Horse");
define("FOOL_DESC","Every 24 hours it gets a random effect, bonus, or penalty (all are possible with the exception of great warehouse, great granary and WW building plans). They change effect AND scope every 24 hours. The unique artifact will always take positive bonuses.");
define("FOOL_SMALL","Artefact of the slight fool");
define("FOOL_SMALLVILLAGE","Pendant of Mischief");
define("FOOL_UNIQUE","Artefact of the unique fool");
define("FOOL_UNIQUEVILLAGE","Forbidden Manuscript");
define("ARTEFACT","Construction plans



Countless days have passed since the first battles upon the walls of the cursed villages of the Dread Natars, many armies of both the free ones and the Natarian empire struggled and died before the walls of the many strongholds from which the Natars had once ruled all creation. Now with the dust settled and a relative calm having settled in, armies began to count their losses and collect their dead, the stench of combat still lingering in the night air, a smell of a slaughter unforgettable in its extent and brutality yet soon to be dwarfed by yet others. The largest armies of the free ones and the Dread Natars were marshalling for yet another renewed assault upon the coveted former strongholds of the Natarian Empire.

Soon scouts arrived telling of a most awesome sight and a chilling reminder, a dread army of an unfathomable size had been spotted marshalling at the end of the world, the Natarian capital, a force so great and unstoppable that the dust from their march would choke off all light, a force so brutal and ruthless that it would crush all hope. The free people knew that they had to race now, race against time and the endless hordes of the Natarian Empire to raise a Wonder of the World to restore the world to peace and vanquish the Natarian threat.

But to raise such a great Wonder would be no easy task, one would need construction plans created in the distant past, plans of such an arcane nature that even the very wisest of sages knew not their contents or locations.

Tens of thousands of scouts roamed across all existence searching in vain for these mystical plans, looking in all places but the dreaded Natarian Capital, yet could not find them. Today however, they return bearing good news, they return baring the locations of the plans, hidden by the armies of the Natars inside secret strongholds constructed to be hidden from the eyes of man.

Now begins the final stretch, when the greatest armies of the Free people and the Natars will clash across the world for the fate of all that lies under heaven. This is the war that will echo across the eons, this is your war, and here you shall etch your name across history, here you shall become legend.


Facts:
To steal one, the following things must happen:
You must attack the village (NO Raid!)
WIN the Attack
Destroy the treasury
An empty treasury lvl 10 MUST be in the village where that attack came from
Have a hero in an attack

If not, the next attack on that village, winning with a hero and empty treasury will take the building plan.

To build a WW, you must own a plan yourself (you = the WW village owner) from lvl 0 to 49, from 50 to 100 you need an additional plan in your alliance! Two plans in the WW village account would not work!

The construction plans are conquerable immediately when they appear to the server. 

There will be a countdown in game, showing the exact time of the release, 5 days prior to the launch. ");


//planos
define("PLAN","Ancient Construction Plan");
define("PLANVILLAGE","WW Buildingplan");
define("PLAN_DESC","With this ancient construction plan you will able to build World Wonder to level 50. to build further, your alliance must hold at least two plans.");
define("PLAN_INFO","World Wonder Construction Plans 


Many moons ago the tribes of Travian were surprised by the unforeseen return of the Natars. This tribe from immemorial times surpassing all in wisdom, might and glory was about to trouble the free ones again. Thus they put all their efforts in preparing a last war against the Natars and vanquishing them forever. Many thought about the so-called 'Wonders of the World', a construction of many legends, as the only solution. It was told that it would render anyone invincible once completed. Ultimately making the constructors the rulers and conquerors of all known Travian. 

However, it was also told that one would need construction plans to construct such a building. Due to this fact, the architects devised cunning plans about how to store these safely. After a while, one could see temple-like buildings in many a city and metropolis - the Treasure Chambers (Treasuries). 

Sadly, no one - not even the wise and well versed - knew where to find these construction plans. The harder people tried to locate them, the more it seemed as if they where only legends. 

Today, however, this last secret will be revealed. Deprivations and endeavors of the past will not have been in vain, as today scouts of several tribes have successfully obtained the whereabouts of the construction plans. Well guarded by the Natars, they lie hidden in several oases to be found all over Travian. Only the most valiant heroes will be able to secure such a plan and bring it home safely so that the construction can begin. 

In the end, we will see whether the free tribes of Travian can once again outwit the Natars and vanquish them once and for all. Do not be so foolish as to assume that the Natars will leave without a fight, though!



To steal a set of Construction Plans from the Natars, the following things must happen:
- You must Attack the village (NOT Raid!)
- You must WIN the Attack
- You must DESTROY the Treasure Chamber (Treasury)
- Your Hero MUST be in that attack, as he is the only one who may carry the Construction Plans
- An empty level 10 Treasure Chamber (Treasury) MUST be in the village where that attack came from
NOTE: If the above criteria is not met during the attack, the next attack on that village which does meet the above criteria will take the Construction Plans.



To build a Treasure Chamber (Treasury), you will need a Main Building level 10 and the village MUST NOT be a Capital or contain a World Wonder.

To build a World Wonder, you must own the Construction Plans yourself (you = the World Wonder Village Owner) from level 0 to 50, and then from level 51 to 100 you will need an additional set of Construction Plans in your Alliance! Two sets of Construction Plans in the World Wonder Village Account will not work!");
define("WWVILLAGE","WW village");
/*
|--------------------------------------------------------------------------
|   Index
|--------------------------------------------------------------------------
*/

	   $lang['index'][0][1] = "欢迎来到 " . SERVER_NAME . "";
	   $lang['index'][0][2] = "用户指南";
	   $lang['index'][0][3] = "这是免费的, 现在就加入！";
	   $lang['index'][0][4] = "什么是 " . SERVER_NAME . "";
	   $lang['index'][0][5] = "" . SERVER_NAME . " 是 <b>网页游戏</b> 是架构于浏览器的游戏<BR />TRAVIAN世界内有数千至数万名玩家，一名玩家就是一個村庄的首领，以外交、权谋、谍战、结盟、分工等等谋略，成为中世纪霸主<br /><p>这是 <strong>免费的</strong> 而且 <strong>不用下载</strong>。";
	   $lang['index'][0][6] = "点击这里玩 " . SERVER_NAME . "";
	   $lang['index'][0][7] = "总人数";
	   $lang['index'][0][8] = "活跃的玩家";
	   $lang['index'][0][9] = "在线的玩家";
	   $lang['index'][0][10] = "关注游戏";
	   $lang['index'][0][11] = "You will begin as the chief of a tiny village and will embark on an exciting quest.";
	   $lang['index'][0][12] = "Build up villages, wage wars or establish trade routes with your neighbours.";
	   $lang['index'][0][13] = "Play with and against thousands of other real players and conquer the the world of Travian.";
	   $lang['index'][0][14] = "新闻";
	   $lang['index'][0][15] = "说明";
	   $lang['index'][0][16] = "游戏截图";
	   $lang['forum'] = "论坛";
	   $lang['register'] = "注册";
	   $lang['login'] = "登录";

//manual
define("INGAME_HELP_TITLE","你可以随时在此处查看游戏帮助");
define("THE_TROOPS","军队");
define("THE_BUILDINGS","建筑");
define("MILITARY","军队");
define("INFRASTRUCTURE","基础设施");
define("VELOCITY","速度");
define("Fields_PER_HOUR","格/小时");
define("CAN_CARRY","负重");
define("DURATION_OF_TRAINING","训练时间");
define("U1_DESC","罗马步兵是罗马帝国最简单通用的兵种。他们全面训练，擅长进攻和防守。但既使通过训练也很达到专业部队的水平。");
define("U2_DESC","禁卫兵是皇帝的卫队。他们不惜生命守护。因为他们专注于训练防守能力，所以他们的攻击非常弱。");

//profile
define("PREFERENCES","偏好设置");
define("PLAYER_PROFILE","玩家资料");
?>
